var express = require('express');
const random = require('random-js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  if ( req.headers["prefer"] === 'json' )
  {
    console.log(req.headers["prefer"]);
    var obj = [{
      id: "root",
      text: "/",
      parent: "#"
    },
    {
      id: "sic",
      text: "DCIM",
      parent: "root",
      children: true
    }
  ]

    //res.send("<ul><li>aaa</li><li>ooo</li></ul>");
    res.json(obj);

    return;
  }
  res.render('desktop/index', { title: 'Express', Random: random });
});

module.exports = router;
