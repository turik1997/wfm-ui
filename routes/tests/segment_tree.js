const express = require('express');
const router = express.Router();

router.get('/segment_tree', function (req, res, next) {
    res.render('tests/segment_tree', {title: 'Tests'});
});

module.exports = router;