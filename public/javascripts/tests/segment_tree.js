var storageC = $('#storage-content');
var total = 0;
var currentTestNumber;

const rowsInput = [0, 1, 1, 1, 0, 100, 1, 2, 5, 100];
const columnsInput = [0, 1, 10, 10, 1, 1, 100, 2, 5, 100];
const itemsInput = [0, 0, 5, 0, 0, 90, 90, 4, 25, 10000];

//TODO implement
function testQueries() {

}

function testItemsCountInResultSet(resultSet, expectedValue) {
    it(expectedValue + " files in the result set of segment tree", function () {
        assert.equal(resultSet.size, expectedValue);
    });
}

function testItems(resultSet, count) {
    it("set has all items from 1 to " + count, function () {
        for ( var i = 1; i <= count; i++ )
            assert(resultSet.has("#file"+i), "#file" + i + " is not in the set");
    });

}

function postfix(number) {
    return number == 1 ? "" : "s";
}

function testTree(rowCount, columnCount, count, testIndex) {
    var info = "#" + testIndex + ": " +
        "building a tree with " +
        rowCount + " row" + postfix(rowCount) + " and " +
        columnCount + " column" + postfix(columnCount) + " and " +
        count + " item" + postfix(count);
    describe(info, function () {

        itemsCount = count;
        rows = rowCount;
        columns = columnCount;

        var resultSet = buildTree(0, 0, rows-1, columns-1, 0);
        testItemsCountInResultSet(resultSet, itemsCount);
        testItems(resultSet, itemsCount);

        if ( testIndex == 7 )
            console.log(resultSet);
    });
}

describe("building a segment tree", function () {

    beforeEach(function () {
        segmentTree = [];
    });

    afterEach(function () {
    });

    for ( var i = 0; i < itemsInput.length; i++ )
    {
        currentTestNumber = i;
        testTree(rowsInput[i], columnsInput[i], itemsInput[i], i);
    }
});