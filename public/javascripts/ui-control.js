var ascending = true;
const HORIZONTAL_MARGIN = 20;
const VERTICAL_MARGIN = 20;
const INNER_WIDTH = 140;
const INNER_HEIGHT = 140;
const ITEM_WIDTH = INNER_WIDTH + 2*HORIZONTAL_MARGIN;
const ITEM_HEIGHT = INNER_HEIGHT + 2*VERTICAL_MARGIN;
const STORAGE_AREA_LEFT_MARGIN = 0;
const STORAGE_AREA_TOP_MARGIN = 50;
var leftBarWidth = 0;
var HEADER_HEIGHT = 60;
const DEBUG_MODE = false;
var selectedFiles = {};

function applyScroll(elementId, direction) {

}

function getTopXIndex(x) {
    var possibleItemIndex = Math.floor(x / ITEM_WIDTH);
    if ( x > (ITEM_WIDTH * possibleItemIndex + HORIZONTAL_MARGIN + INNER_WIDTH) )
        possibleItemIndex++;
    return possibleItemIndex;
}

function getBottomXIndex(x) {
    var possibleItemIndex = Math.floor(x / ITEM_WIDTH);
    if ( x < (ITEM_WIDTH * possibleItemIndex + HORIZONTAL_MARGIN) )
        possibleItemIndex--;
    return possibleItemIndex;
}

function getTopYIndex(y) {
    var possibleItemIndex = Math.floor(y / ITEM_HEIGHT);
    //console.log("possibleTop: " + possibleItemIndex);
    //console.log("End of possible: " + (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN + INNER_HEIGHT) );
    if ( y > (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN + INNER_HEIGHT) )
        possibleItemIndex++;
    return possibleItemIndex;
}

function getBottomYIndex(y) {
    var possibleItemIndex = Math.floor(y / ITEM_HEIGHT);
    //console.log("possibleBottom: " + possibleItemIndex);
    //console.log("begin of possible: " + (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN) + " but y: " + y);
    if ( y < (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN) )
        possibleItemIndex--;
    return possibleItemIndex;
}

var ascendingOrder = true;
var storageContent = null;
var selectionStarted = false;
var itemsCount = 0;

function getMousePosition(event){
    //console.log("x: " + event.clientX + " y: " + event.clientY);
    return {x:(event.clientX+$('#storage-content').prop('scrollLeft')-$('#left-bar').prop('offsetWidth')-$('#storage-content').prop('offsetLeft')),
        y: (event.clientY+$('#storage-content').prop('scrollTop')-$('#storage-content').prop('offsetTop') - 60)};//TODO change 60 to header height
}

function makeSegmentTreeQuery(rect) {
    return {
        'startRow': getTopYIndex(rect.p1.y),
        'startColumn': getTopXIndex(rect.p1.x),
        'endRow': getBottomYIndex(rect.p2.y),
        'endColumn': getBottomXIndex(rect.p2.x)
    };
}

function makeRectangle(point1, point2) {
    return {
        'p1': point1,
        'p2': point2
    };
}

function makePoint(x, y) {
    return {
        'x': x,
        'y': y
    };
}

/*function getItemsVisibleOnScreen(scrollableItemsContent) {

    if ( !scrollableItemsContent )
        return emptySet();

    var topY = scrollableItemsContent.prop('scrollTop') - STORAGE_AREA_TOP_MARGIN;
    var topX = scrollableItemsContent.prop('scrollLeft') - STORAGE_AREA_LEFT_MARGIN;
    var point1 = makePoint(topX, topY);

    var bottomY = topY + scrollableItemsContent.prop('offsetHeight') - 1;
    var bottomX = topX + scrollableItemsContent.prop('offsetWidth') - 1;
    var point2 = makePoint(bottomX, bottomY);

    var treeQuery = makeSegmentTreeQuery(makeRectangle(point1, point2));

    return getItems(0, 0, rows-1, columns-1, treeQuery, 0);
}*/

function isLeftButton(event) {
    return event.which && event.which == 1;
}

function isRightButton(event) {
    return event.which && event.which == 3;
}



function prepareStorageContent(storageContent) {

    function selectFiles(x1, y1, x2, y2, callback) {

        if ( !callback )
            callback = function (item) {};

        var areaWidth = storageContent.prop('scrollWidth') - STORAGE_AREA_LEFT_MARGIN;
        var areaHeight = storageContent.prop('scrollHeight') - STORAGE_AREA_TOP_MARGIN;
        columns = Math.floor(areaWidth / 180);
        rows = Math.floor(areaHeight / 180);
        if ( itemsCount < columns )
            columns = itemsCount;

        var array = [];
        var startRow = getTopYIndex(y1);
        var startColumn = getTopXIndex(x1);
        var endRow = getBottomYIndex(y2);
        var endColumn = getBottomXIndex(x2);
        if ( DEBUG_MODE )
            console.log("Selecting files." +
                " startRow: " + startRow +
                " startColumn: " + startColumn +
                " endRow: " + endRow +
                " endColumn: " + endColumn);

        if ( startRow > endRow || startColumn > endColumn )
            return array;

        for ( var i = startRow; i <= endRow; i++ )
        {
            for ( var j = startColumn; j <= endColumn; j++ )
            {
                var id = i * columns + j + 1;
                if ( id > itemsCount )
                    continue;
                var fileId = '#file' + id;
                callback(fileId);
                array.push(fileId);
            }
        }

        return array;
    }

    function configMouseMovements(storageContent) {

        function redrawMouseSelection(topX, topY, bottomX, bottomY) {
            mouseSelection.css('visibility', 'visible');
            mouseSelection.css('left', topX);
            mouseSelection.css('top', topY);
            mouseSelection.css('width', bottomX-topX);
            mouseSelection.css('height', bottomY-topY);
        }

        function stopMouseSelection() {
            mouseSelection.css('width', 0);
            mouseSelection.css('height', 0);
            mouseSelection.css('visibility', 'hidden');
        }

        function onItemMouseD(itemId, event, cursorPoint) {
            var clickEvent;
            if ( isLeftButton(event) )
            {
                clickEvent = $.Event('leftclick', cursorPoint);
            }
            else
            if ( isRightButton(event) )
            {
                clickEvent = $.Event('rightclick', cursorPoint);
            }
            else
            {
                clickEvent = event;
            }

            $(itemId).trigger(clickEvent);
        }

        function getMouseDownedItem(cursorPoint) {
            if ( !cursorPoint )
                return null;

            var possibleItemId = null;
            var array = selectFiles(cursorPoint.x - STORAGE_AREA_LEFT_MARGIN, cursorPoint.y - STORAGE_AREA_TOP_MARGIN,
                                    cursorPoint.x - STORAGE_AREA_LEFT_MARGIN, cursorPoint.y - STORAGE_AREA_TOP_MARGIN);
            if ( array.length > 0 )
                possibleItemId = array[0];

            return possibleItemId;
        }

        var isMouseDownedOnFile = false;
        var isMouseDownedOnEmptyArea = false;
        var isDraggingFiles = false;
        var isSelectingFiles = false;
        var mouseDownedFile = null;
        var mouseDownedPosition = null;
        var draggedFiles = [];

        const DRAG_EPSILON = 4;
        var timer = null;


        storageContent.mousedown(function(event){
            event.preventDefault();
            hideSortMenu();
            var cursorPoint = getMousePosition(event);
            mouseDownedPosition = cursorPoint;

            //var possibleItemsToClickOn = getItemsVisibleOnScreen(storageContent);
            var itemId = getMouseDownedItem(cursorPoint);

            if ( itemId )
            {
                isMouseDownedOnFile = true;
                mouseDownedFile = itemId;

                var selectedCount = $('#selected-count').html();
                if ( !isNaN(selectedCount) && selectedCount > 1 && $(mouseDownedFile).hasClass('selected') )
                {
                    return;
                }

                if ( !event.ctrlKey )
                {
                    for ( key in selectedFiles )
                    {
                        $(key).removeClass('selected');
                        delete selectedFiles[key];
                    }

                    selectedFiles = {};
                }
                $('.last-selected').removeClass('last-selected');
                $(mouseDownedFile).addClass('selected').addClass('last-selected');
            }

            if ( isLeftButton(event) )
            {
                $('.menu').css('display', 'none');
                if ( itemId )
                    return;

                $('.last-selected').removeClass('last-selected');
                if ( !event.ctrlKey )
                {
                    for ( key in selectedFiles )
                    {
                        if ( DEBUG_MODE )
                            console.log("deselecting " + key);
                        $(key).removeClass('selected');
                        delete selectedFiles[key];
                    }

                    selectedFiles = {};
                }

                isMouseDownedOnEmptyArea = true;
                return;
            }

            if ( isRightButton(event) )
            {
                event.preventDefault();
                event.stopPropagation();

                $('.menu').css('display', 'none');
                if ( !itemId )
                {
                    $('.selected').removeClass('last-selected').removeClass('selected');
                    selectedFiles = {};
                    var contextMenu = $('#empty-context-menu');
                    contextMenu.css('left', event.clientX);
                    contextMenu.css('top', event.clientY);
                    contextMenu.css('display', 'block');
                    return;
                }

                var contextMenu = $('#file-context-menu');
                contextMenu.css('left', event.clientX);
                contextMenu.css('top', event.clientY);
                contextMenu.css('display', 'block');
                return;
            }

            if ( DEBUG_MODE )
                console.log("No item is clicked. Must be a selection");



        });

        function isDrag(mouseDownedPosition, cursorPosition) {
            return Math.abs(mouseDownedPosition.x - cursorPosition.x) >= DRAG_EPSILON ||
                Math.abs(mouseDownedPosition.y - cursorPosition.y) >= DRAG_EPSILON;
        }

        storageContent.mousemove(function (event) {
            event.preventDefault();


            var cursorPosition = getMousePosition(event);

            if ( isDraggingFiles )
            {
                //TODO animation of dragging
            }

            if ( isMouseDownedOnFile )
            {

                if ( isDrag(mouseDownedPosition, cursorPosition) )
                {
                    isDraggingFiles = true;
                    //TODO (animation or registering) of dragged files
                }

                return;
            }

            if ( isSelectingFiles )
            {
                var topX = Math.min(mouseDownedPosition.x, cursorPosition.x);
                var bottomX = Math.max(mouseDownedPosition.x, cursorPosition.x);
                var topY = Math.min(mouseDownedPosition.y, cursorPosition.y);
                var bottomY = Math.max(mouseDownedPosition.y, cursorPosition.y);

                redrawMouseSelection(topX, topY, bottomX, bottomY);

                if ( !event.ctrlKey )
                {
                    $('.selected').removeClass('selected');
                    selectedFiles = {};
                }
                else
                {
                    $('.selected').removeClass('selected');
                    for ( key in selectedFiles )
                        $(key).addClass('selected');
                }
                selectFiles(topX-STORAGE_AREA_LEFT_MARGIN, topY-STORAGE_AREA_TOP_MARGIN,
                    bottomX-STORAGE_AREA_LEFT_MARGIN, bottomY-STORAGE_AREA_TOP_MARGIN, function (item) {
                        if ( selectedFiles[item] )
                        {
                            $(item).removeClass('selected');
                        }
                        else
                        {
                            $(item).addClass('selected');
                        }
                    });

                //console.log(selectedSet);
                /*selectedSet.forEach(function(item){
                    $(item).addClass('selected');
                    //console.log(item + " changed color");
                    selectedFiles.push(item);
                });*/
                $('#selected-count').html($('.selected').length);
                return;
            }

            if ( isMouseDownedOnEmptyArea )
            {
                /*
                var topX = Math.min(mouseDownedPosition.x, cursorPosition.x);
                var bottomX = Math.max(mouseDownedPosition.x, cursorPosition.x);
                var topY = Math.min(mouseDownedPosition.y, cursorPosition.y);
                var bottomY = Math.max(mouseDownedPosition.y, cursorPosition.y);

                selectFiles(topX-STORAGE_AREA_LEFT_MARGIN, topY-STORAGE_AREA_TOP_MARGIN,
                    bottomX-STORAGE_AREA_LEFT_MARGIN, bottomY-STORAGE_AREA_TOP_MARGIN);*/
                if ( isDrag(mouseDownedPosition, cursorPosition) )
                    isSelectingFiles = true;
                return
            }
        });


        var scrollingtimer = null;
        var diff = 0;


        $('#header').mousemove(function (event) {
            diff = HEADER_HEIGHT - event.clientY;
        });

        $('#header').mouseleave(function (event) {
            clearInterval(scrollingtimer);
            scrollingtimer = null;
        });
        $('#header').mouseenter(function (event) {

            if ( !isMouseDownedOnEmptyArea )
                return;

            if ( storageContent.prop('scrollTop') == 0 )
                return;

            if ( isSelectingFiles )
            {
                diff = HEADER_HEIGHT - event.clientY;

                if ( !scrollingtimer )
                    scrollingtimer = setInterval(function () {
                        storageContent.get(0).scrollTop -= (diff * 342) / HEADER_HEIGHT;
                    }, 100);
                return;
            }

            var cursorPosition = getMousePosition(event);
            if ( isDrag(mouseDownedPosition, cursorPosition) )
            {
                isSelectingFiles = true;
                console.log("ahaha");
                // TODO redrawMouseSelection();
                scrollingtimer = setInterval(function () {
                    console.log("booo");
                    storageContent.get(0).scrollTop -= (diff * 114) / HEADER_HEIGHT;
                }, 100);
                return;
            }

        });

        storageContent.mouseup(function (event) {
            event.preventDefault();
            var cursorPosition = getMousePosition(event);
            if ( DEBUG_MODE )
                console.log("mouse is up");

            if ( isSelectingFiles )
            {
                isMouseDownedOnEmptyArea = false;
                isSelectingFiles = false;
                selectedFiles = {};
                var count = 0;
                $('.selected').each(function () {
                    selectedFiles['#' + $(this).attr('id')] = true;
                    count++;
                });
                $('#selected-count').html(count);

                stopMouseSelection();
                return;
            }

            if ( isDraggingFiles )
            {
                isDraggingFiles = false;
                isMouseDownedOnFile = false;
                var count = 0;
                $('.selected').each(function () {
                    selectedFiles['#' + $(this).attr('id')] = true;
                    count++;
                });
                $('#selected-count').html(count);
                //stopDragging();
                return;
            }

            if ( isMouseDownedOnFile )
            {
                isMouseDownedOnFile = false;

                if ( event.ctrlKey )
                {
                    if ( selectedFiles[mouseDownedFile] )
                    {
                        $(mouseDownedFile).removeClass('last-selected').removeClass('selected');
                        delete selectedFiles[mouseDownedFile];
                    }
                    else
                    {
                        selectedFiles[mouseDownedFile] = true;
                    }
                }
                else
                {
                    if ( !selectedFiles[mouseDownedFile] )
                        selectedFiles[mouseDownedFile] = true;
                    else
                    {
                        for ( key in selectedFiles )
                        {
                            $(key).removeClass('selected');
                            delete selectedFiles[key];
                        }

                        selectedFiles = {};

                        $('.last-selected').removeClass('last-selected');
                        $(mouseDownedFile).addClass('selected').addClass('last-selected');
                        selectedFiles[mouseDownedFile] = true;
                    }
                }




                $('#selected-count').html($('.selected').length);
                return;
            }

            if ( isMouseDownedOnEmptyArea )
            {
                isMouseDownedOnEmptyArea = false;
                $('#selected-count').html($('.selected').length);
                return;
            }

        });
    }

    function configSegmentTree(storageContent) {
        itemsCount = storageContent.children().length-1; //1 child is the mouse-selection
        var areaWidth = storageContent.prop('scrollWidth') - STORAGE_AREA_LEFT_MARGIN;
        var areaHeight = storageContent.prop('scrollHeight') - STORAGE_AREA_TOP_MARGIN;

        //TODO substitute dynamically calculated sizes for 160
        columns = Math.floor(areaWidth / 160);
        rows = Math.floor(areaHeight / 160);
        if ( itemsCount < columns )
            columns = itemsCount;

        buildTree(0, 0, rows-1, columns-1, 0);
    }

    function configStorageContentScrolling(storageContent) {

        function isScrollDown(previousPos, currentPos) {
            return previousPos - currentPos < 0;
        }

        $(window).bind('mousewheel DOMMouseScroll', function(event)
        {
            if(event.ctrlKey == true)
            {
                event.preventDefault();
            }
        });

        var upperBound = $('#upper-bound');
        var previousPosition = 0;
        storageContent.scroll(function(event){
            if ( $('.context-menu').css('display') !== 'none' )
            {
                event.preventDefault();
                event.stopPropagation();
                $(this).get(0).scrollTop = previousPosition;
            }
            var currentPosition = $(this).prop('scrollTop');
            if ( !isScrollDown(previousPosition, currentPosition) )
            {
                if ( currentPosition > 0 )
                    upperBound.addClass('bound-overlay')
                else
                    upperBound.removeClass('bound-overlay');

                upperBound.removeClass('hidden-bound');
                upperBound.addClass('fixed-bound');
            }
            else
            {
                upperBound.removeClass('fixed-bound');
                upperBound.addClass('hidden-bound');
            }

            previousPosition = currentPosition;
        });
    }

    var mouseSelection = $('#mouse-selection')
    configSegmentTree(storageContent);
    configStorageContentScrolling(storageContent);
    configMouseMovements(storageContent);
    var timer;

    var startedAt, currentPos;

}

function showSortMenu() {
    $('#sort-by-menu').css('display', 'block');
}

function hideSortMenu() {
    $('#sort-by-menu').css('display', 'none');
}

function prepareInterface() {
    $('#sort-by-btn').click(function (event) {
        var sortByMenu = $('#sort-by-menu');
        if ( sortByMenu.css('display') !== 'none' )
            hideSortMenu();
        else
            showSortMenu();
    });

    $('.btn-copy').click(function (event) {
        $.get('/api/copy');
    });
}

$(document).ready(function(){
    var mouseSelectionArea = $('.mouse-selection');
    storageContent = $('#storage-content');
    //var orderButtonIcon = $('#order-btn-icon');
    $(this).on('contextmenu', function(event){
        event.preventDefault();
    });

    $.get({
      url: window.location.href,
      headers: {
        Prefer: 'json'
      },
      success: function (dirContent) {
        var content = JSON.parse(dirContent);
        console.log(content);
      },
      error: function (err) {

      }
    });
    prepareStorageContent(storageContent);
    prepareInterface();


});

$(window).load(function () {

    $(function () {
      $('#jstree_demo_div').jstree({
        'core' : {
            'data' : {
                'url' : '/get_tree',
                'data': function(node) {
                  var start = node.id === '#' ? '/' : get_path(node, '/');
                  var end = node.id === '#' ? window.location.href.substr('http://localhost:3000'.length) : get_path(node, '/');
                  return JSON.stringify({ 'start': start, 'end': end });
                },
                'method': 'POST',
                'success': function (a) {
                  a.forEach(console.log);
                }
            }
        }
      });

    });


});
