var segmentTree = [];
var columns = 2;
var rows = 2;
var itemsCount = 0;

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

function buildTree(startRow, startColumn, endRow, endColumn, node) {


    //console.log(arguments);
    if ( startRow > endRow || startColumn > endColumn )
        return emptySet();

    if ( startRow == endRow && startColumn == endColumn )
    {
        var id = endRow * columns + startColumn + 1;
        if ( typeof TEST_MODE !== 'undefined' && TEST_MODE )
        {
            if ( startRow == 1 && startColumn == 1 )
                console.log(startRow + ";" + startColumn + " id: " + id + " #" + currentTestNumber);
            if ( startRow == 1 && startColumn == 0 )
                console.log(startRow + ";" + startColumn + " id: " + id + " #" + currentTestNumber);
        }

        if ( id > itemsCount )
            return emptySet();
        var fileId = '#file' + id;
        //console.log(fileId);
        var resultSet = emptySet();
        resultSet.add(fileId);

        return segmentTree[node] = resultSet;
    }

    if ( (startRow == endRow && (startRow * columns + startColumn + 1) > itemsCount) )
    {
        return emptySet();
    }


    var middleRow = Math.floor((startRow + endRow) / 2);
    var middleColumn = Math.floor((startColumn + endColumn) / 2);

    var childIndex = 4*node;
    var sets = [];
    sets[0] = buildTree(startRow, startColumn, middleRow, middleColumn, childIndex+1);
    sets[1] = buildTree(startRow, middleColumn+1, middleRow, endColumn, childIndex+2);
    sets[2] = buildTree(middleRow+1, startColumn, endRow, middleColumn, childIndex+3);
    sets[3] = buildTree(middleRow+1, middleColumn+1, endRow, endColumn, childIndex+4);

    var resultSet = merge(sets);

    segmentTree[node] = resultSet;
    return segmentTree[node];
}

function getItems(startRow, startColumn, endRow, endColumn, rangeStartRow, rangeStartColumn, rangeEndRow, rangeEndColumn, node) {
    //console.log(arguments);

    function getTreeItems(startRow, startColumn, endRow, endColumn, rangeStartRow, rangeStartColumn, rangeEndRow, rangeEndColumn, node) {
        if ( rangeStartRow < startRow )
            rangeStartRow = startRow;
        if ( rangeEndRow > endRow )
            rangeEndRow = endRow;
        if ( rangeStartColumn < startColumn )
            rangeStartColumn = startColumn;
        if ( rangeEndColumn > endColumn )
            rangeEndColumn = endColumn;

        if ( startRow == rangeStartRow && startColumn == rangeStartColumn
            && endRow == rangeEndRow && endColumn == rangeEndColumn )
        {
            //console.log(segmentTree[node]);
            return segmentTree[node];
        }

        if ( rangeStartRow > endRow || startRow > rangeEndRow
            || rangeStartColumn > endColumn || startColumn > rangeEndColumn
            || rangeStartColumn > rangeEndColumn || rangeStartRow > rangeEndRow )
            return emptySet();
        var middleRow = Math.floor((startRow + endRow) / 2);
        var middleColumn = Math.floor((startColumn + endColumn) / 2);

        var childIndex = 4*node;
        var sets = [];
        sets[0] = getTreeItems(startRow, startColumn, middleRow, middleColumn, rangeStartRow, rangeStartColumn, Math.min(middleRow, rangeEndRow), Math.min(middleColumn, rangeEndColumn), childIndex+1);
        sets[1] = getTreeItems(startRow, middleColumn+1, middleRow, endColumn, rangeStartRow, Math.max(middleColumn+1, rangeStartColumn), Math.min(middleRow, rangeEndRow), rangeEndColumn, childIndex+2);
        sets[2] = getTreeItems(middleRow+1, startColumn, endRow, middleColumn, Math.max(middleRow+1, rangeStartRow), rangeStartColumn, rangeEndRow, Math.min(middleColumn, rangeEndColumn), childIndex+3);
        sets[3] = getTreeItems(middleRow+1, middleColumn+1, endRow, endColumn, Math.max(middleRow+1, rangeStartRow), Math.max(middleColumn+1, rangeStartColumn), rangeEndRow, rangeEndColumn, childIndex+4);
        var result = merge(sets);
        sets = null;
        return result;
    }

    //console.log(arguments);
    if ( isNaN(startRow) || isNaN(startColumn) || isNaN(endRow) || isNaN(endColumn) ||
        (isNaN(rangeStartRow) && !(rangeStartRow instanceof Object)) || isNaN(rangeStartColumn) )
        return emptySet();

    if ( arguments.length == 6 && ( arguments[4] instanceof Object ) && !isNaN(arguments[5]) )
    {
        var query = arguments[4];
        rangeStartRow = query.startRow;
        rangeStartColumn = query.startColumn;
        rangeEndRow = query.endRow;
        rangeEndColumn = query.endColumn;
        node = arguments[5];
        //console.log(arguments);
    }

    if ( isNaN(rangeEndRow) || isNaN(rangeEndColumn) || isNaN(node) )
        return emptySet();

    return getTreeItems(startRow, startColumn, endRow, endColumn, rangeStartRow, rangeStartColumn, rangeEndRow, rangeEndColumn, node);
}

function merge(results) {
    var resultSet = emptySet();
    results.forEach(function(set){
        if ( !set )
            return;

        set.forEach(function(item){
            resultSet.add(item);
        });
    });



    return resultSet;
}

function emptySet() {
    return new Set();
}